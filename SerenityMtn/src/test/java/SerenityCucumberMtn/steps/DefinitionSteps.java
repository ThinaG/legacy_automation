package SerenityCucumberMtn.steps;

import cucumber.api.PendingException;
import cucumber.api.java.en.And;
import net.thucydides.core.annotations.Steps;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

import SerenityCucumberMtn.steps.serenity.EndUserSteps;

public class DefinitionSteps {

    @Steps
    EndUserSteps anna;

    @Given("^I navigate to the login page$")
    public void iNavigateToTheLoginPage() throws Throwable {
        anna.is_the_home_page();
    }

    @And("^I enter the username as \"([^\"]*)\" nd password as \"([^\"]*)\"$")
    public void iEnterTheUsernameAsAdminNdPasswordAsAdmin(String username, String password) throws Throwable {
        anna.enters_username(username);
    }

    @And("^I click login button$")
    public void iClickLoginButton() throws Throwable {
        anna.clicks_submitbutton();
    }

    @And("^I enter the username as agent nd password as agent$")
    public void iEnterTheUsernameAsAgentNdPasswordAsAgent() throws Throwable {
        // Write code here that turns the phrase above into concrete actions
        throw new PendingException();
    }

    @And("^I enter the username as superadmin nd password as superadmin$")
    public void iEnterTheUsernameAsSuperadminNdPasswordAsSuperadmin() throws Throwable {
        // Write code here that turns the phrase above into concrete actions
        throw new PendingException();
    }

    @And("^I enter the username as superagent nd password as superagent$")
    public void iEnterTheUsernameAsSuperagentNdPasswordAsSuperagent() throws Throwable {
        // Write code here that turns the phrase above into concrete actions
        throw new PendingException();
    }

    @Given("^I navigate to the termination reason$")
    public void iNavigateToTheTerminationReason() throws Throwable {
        // Write code here that turns the phrase above into concrete actions
        throw new PendingException();
    }


    @And("^I enter the termination reason notes$")
    public void iEnterTheTerminationReasonNotes() throws Throwable {
        // Write code here that turns the phrase above into concrete actions
        throw new PendingException();
    }

    @Then("^Select to create a cancellation note$")
    public void selectToCreateACancellationNote() throws Throwable {
        // Write code here that turns the phrase above into concrete actions
        throw new PendingException();
    }

    @And("^An early termination Calculation is completed$")
    public void anEarlyTerminationCalculationIsCompleted() throws Throwable {
        // Write code here that turns the phrase above into concrete actions
        throw new PendingException();
    }

    @Given("^I navigate to the View and Confirm Early termination details page$")
    public void iNavigateToTheViewAndConfirmEarlyTerminationDetailsPage() throws Throwable {
        // Write code here that turns the phrase above into concrete actions
        throw new PendingException();
    }

    @And("^I view the early termination details$")
    public void iViewTheEarlyTerminationDetails() throws Throwable {
        // Write code here that turns the phrase above into concrete actions
        throw new PendingException();
    }

    @And("^I confirm the early termination calculation for the consumer$")
    public void iConfirmTheEarlyTerminationCalculationForTheConsumer() throws Throwable {
        // Write code here that turns the phrase above into concrete actions
        throw new PendingException();
    }

    @Then("^I send the report$")
    public void iSendTheReport() throws Throwable {
        // Write code here that turns the phrase above into concrete actions
        throw new PendingException();
    }
}
