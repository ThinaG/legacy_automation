package SerenityCucumberMtn.steps.serenity;

import SerenityCucumberMtn.steps.serenity.EndUserSteps;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import net.thucydides.core.annotations.Steps;

public class TerminationStepDefs {

    @Steps
    EndUserSteps anna;

    public void beforeOpen() {
        anna.goMax();
    }


    @And("^I enter the username as \"([^\"]*)\"$")
    public void i_enter_the_username_as(String username) throws Exception {
        anna.enters_username(username);

    }

//    @And("^I click login button$")
//    public void iClickLoginButton() throws Throwable {
//        anna.clicks_submitbutton();
//    }

    @And("^I navigate to search term page$")
    public void I_navigate_to_search_term_page() throws Throwable {
        anna.navigates_To_Search_Subscriber_Page();
    }

    @And("^I enter the account number \"([^\"]*)\"$")
    public void I_enter_the_account_number(String accountNumber) throws Throwable {
        anna.enters_AccountNumber(accountNumber);
    }


    @When("^I navigate to the early-termination page$")
    public void i_navigate_to_the_early_termination_page() throws Exception {
        anna.navigates_To_early_termination_page();
    }

    @And("I select the termination reason$")
    public void i_select_the_termination_reason() {
        anna.selects_termination_reason();
    }

    @When("^I enter the termination reason note \"([^\"]*)\"$")
    public void i_enter_the_termination_reason_note(String reason) throws Exception {
        anna.enters_termination_note_reason(reason);
    }


    @And("^I select to create a cancellation note$")
    public void i_select_to_create_a_cancellation_note() throws Exception {
        anna.selects_cancellation_note_type_and_submits();
    }

    @And("^I click the submit button$")
    public void i_click_the_submit_button() throws Exception {
        // Write code here that turns the phrase above into concrete actions
        anna.clicks_submitbutton();

    }

    //@Then("^I should be able to see a consumer early termination page$")
    //public void i_should_be_able_to_see_a_consumer_early_termination_page() throws Exception {
     //     anna.navigates_To_early_termination_page();
    }



