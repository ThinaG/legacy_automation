package SerenityCucumberMtn.steps.serenity;

import SerenityCucumberMtn.pages.MtnPageObjects;
import net.thucydides.core.annotations.Step;

import static net.thucydides.core.webdriver.ThucydidesWebDriverSupport.getDriver;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.containsString;
import static org.hamcrest.Matchers.hasItem;

public class EndUserSteps {

    MtnPageObjects mtnPageObjects;

    @Step
    public void goMax() {
        mtnPageObjects.getDriver().manage().window().maximize();
    }


    @Step
    public void is_the_home_page() {
        mtnPageObjects.open();
    }

    @Step
    public void enters_username(String username) {
        mtnPageObjects.enter_username(username);
    }

    @Step
    public void enters_password(String password) {
        mtnPageObjects.enter_password(password);
    }

    @Step
    public void clicks_submitbutton() {
        mtnPageObjects.clicks_button();
    }

    //=========================================Termination Steps

    @Step
    public void navigates_To_Search_Subscriber_Page() {
        mtnPageObjects.navigates_to_searchSubscriber();
    }

    @Step
    public void enters_AccountNumber(String accountNo) {
        mtnPageObjects.enters_accountNo(accountNo);
    }

    @Step
    public void navigates_To_early_termination_page() {
        mtnPageObjects.navigate_to_the_early_termination_page();
    }

    @Step
    public void selects_termination_reason() {
        mtnPageObjects.select_termination_reason();
    }

    @Step
    public String enters_termination_note_reason(String reason) {
        return mtnPageObjects.enter_termination_reason(reason);
    }

    @Step
    public void selects_cancellation_note_type_and_submits() {
        mtnPageObjects.select_termination_note_type();
    }




    //================================================ View and Confirm Details

    @Step
    public void I_Click_Submit_Button() {
        mtnPageObjects.clicks_button();
    }

    @Step
    public void View_Confirm_Details(String view) {
        mtnPageObjects.navigate_to_the_early_termination_page();
    }
}





