package SerenityCucumberMtn.steps.serenity;
import SerenityCucumberMtn.steps.serenity.EndUserSteps;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import net.thucydides.core.annotations.Steps;

public class ViewAndConfirmSteps {
    @Steps
    EndUserSteps anna;

    public void beforeOpen() {
        anna.goMax();
    }

    //@And("^I select to create a cancellation note$")
    //public void i_select_to_create_a_cancellation_note() throws Exception {
    //  anna.selects_cancellation_note_type_and_submits();
    //}

    //@When("^I click the submit button$")
    //public void i_click_the_submit_button() throws Exception {
    // Write code here that turns the phrase above into concrete actions
    //  anna.clicks_submitbutton();

//}

    @Then("^I should be able to view and confirm cancellation details$")
    public void i_should_be_able_to_view_and_confirm_cancellation_details() throws Exception {
        // Write code here that turns the phrase above into concrete actions
        anna.navigates_To_early_termination_page();
    }

}
