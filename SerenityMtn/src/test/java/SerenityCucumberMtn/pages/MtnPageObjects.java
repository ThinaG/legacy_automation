package SerenityCucumberMtn.pages;

import net.thucydides.core.annotations.DefaultUrl;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import net.serenitybdd.core.pages.WebElementFacade;
import java.util.stream.Collectors;

import net.serenitybdd.core.annotations.findby.FindBy;

import net.thucydides.core.pages.PageObject;

import java.util.List;

@DefaultUrl("https://mycontract-qa1.mtn.co.za/landing/landing.htm")
public class MtnPageObjects extends PageObject {

    //login elements
    @FindBy(id="username")
    private WebElementFacade username;

    @FindBy(className="banner-textfield")
    private WebElementFacade password;

    @FindBy(id="submitButton")
    private  WebElementFacade submitButton;

    public void enter_username(String keyword) {
        username.type(keyword);
    }

    public void enter_password(String password) {
        username.type(password);
    }

    public void clicks_button(){
        submitButton.click();
    }

    //==================================================================================================
    //Termination navigation
    @FindBy(xpath = "/html/body/div[3]/div[2]/div[3]/div/div[3]/a")
    private  WebElementFacade search_subscriber;

    @FindBy(xpath = "/html/body/div[3]/div[2]/div[3]/form/div[1]/table/tbody/tr[1]/td[2]/ul/li[2]/span[1]")
    private  WebElementFacade account_radio_button;


    @FindBy(id = "accountNo")
    private WebElementFacade accountNo;

    @FindBy(id = "submitForm")
    private WebElementFacade submitForm;

    @FindBy(xpath = "/html/body/div[3]/div[2]/div[3]/div[1]/div/div/table/tbody/tr/td[5]/a")
    private WebElementFacade loginButton;

    @FindBy(xpath ="//*[@id=\"oneKeyQuestionAnsweredCorrectly\"]")
    private WebElementFacade oneKey;

    @FindBy(xpath = "/html/body/div[3]/div[2]/div/ul/li[4]/a")
    private WebElementFacade MyServiceTab;

    @FindBy(xpath = "/html/body/div[3]/div[2]/div/ul/li[4]/a")
    private  WebElementFacade myServices;

    @FindBy(xpath = "/html/body/div[3]/div[2]/div/ul/li[4]/div/div[3]/p[5]/a")
    private WebElementFacade earlyTermination;

    @FindBy(xpath = "//*[@id=\"terminationReasonDescription_title\"]")
    private  WebElementFacade termination_reason;

    @FindBy(xpath = "/html/body/div[3]/div[3]/div[3]/div[4]/form/table/tbody/tr[1]/td[2]/div[2]/div[2]/ul/li[3]")
    private WebElementFacade poor_service;

    @FindBy(id = "optionalNote")
    private WebElementFacade note;

    @FindBy(xpath = "/html/body/div[3]/div[3]/div[3]/div[4]/form/table/tbody/tr[3]/td[2]/ul/li[1]/span[1]")
    private WebElementFacade termination_note_type_cancellation;

    @FindBy(xpath = "//*[@id=\"submitButton\"]")
    WebElementFacade submitTermination;




    public void navigates_to_searchSubscriber(){
        search_subscriber.click();
        account_radio_button.click();
    }

    public void enters_accountNo(String accountNumber){
        accountNo.type(accountNumber);
    }

    public void navigate_to_the_early_termination_page()
    {
        submitForm.click();
        loginButton.click();
        oneKey.click();
        submitForm.click();
        myServices.click();
        earlyTermination.click();

    }

    public void select_termination_reason(){
        termination_reason.click();
        poor_service.click();
    }

    public String enter_termination_reason(String reason){
         note.type(reason);
         return reason;
    }

    public void select_termination_note_type(){
        termination_note_type_cancellation.click();
        submitTermination.click();
    }

}