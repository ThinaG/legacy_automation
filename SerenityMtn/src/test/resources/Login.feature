Feature: LoginFeature

  This feature deals with the login functionality of the application

  Scenario: Login with correct username and password
    Given I navigate to the login page
    And I enter the username as "admin" nd password as "admin"
    And I click login button
    Then I should see the CSR page
    And I should be able to see a consumer early termination page



  Scenario: Login with correct username and password
    Given I navigate to the login page
    And I enter the username as "agent" nd password as "agent"
    And I click login button
    Then I should see the CSR page
    And I should be able to see a consumer early termination page


  Scenario: Login with correct username and password
    Given I navigate to the login page
    And I enter the username as "superadmin" nd password as "superadmin"
    And I click login button
    Then I should see the CSR page
    And I should be able to see a consumer early termination page


  Scenario: Login with correct username and password
    Given I navigate to the login page
    And I enter the username as "superagent" nd password as "superagent"
    And I click login button
    Then I should see the CSR page
    And I should be able to see a consumer early termination page