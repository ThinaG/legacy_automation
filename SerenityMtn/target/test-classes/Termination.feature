

Feature: LoginFeature

  This feature deals with the login functionality of the application

  @Thina
  Scenario: Login with correct username and password
    Given I navigate to the login page
    And I enter the username as "abiram_c_103332"
    And I click login button
    When I navigate to search term page
    And I enter the account number "A0007345"
    And I navigate to the early-termination page
    And I select the termination reason
    And I enter the termination reason note "I don't like the service because it's slow"
    And I select to create a cancellation note
    And I click the submit button
