<!DOCTYPE html><html xmlns="http://www.w3.org/1999/xhtml"><head>
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <title>MTN Active</title>
    <link href="/resources/css/fonts.css" rel="stylesheet" type="text/css" media="screen" />
<link href="/resources/css/styles.css" rel="stylesheet" type="text/css" />
<link href="/resources/css/customizations.css" rel="stylesheet" type="text/css" media="screen" />
<link href="/resources/css/MTNactive.css" rel="stylesheet" type="text/css" />
<link href="/resources/css/colorbox.css" rel="stylesheet" media="screen" />
<link href="/resources/css/glossary.css" rel="stylesheet" media="screen" />
<link href="/resources/css/print.css" rel="stylesheet" media="print" />
<link href="/resources/css/li-scroller.css" rel="stylesheet" type="text/css" />

<!--[if lt IE 8]>
		<link href="/resources/css/ie7.css" rel="stylesheet" type="text/css" media="screen" />
		<![endif]-->
		<!--[if lt IE 9]>
			<link href="/resources/css/border_radius_fixes.css" rel="stylesheet" type="text/css" media="screen" />
		<![endif]-->
		<!--[if IE]>
		<link href="/resources/css/ie.css" rel="stylesheet" type="text/css" media="screen" />
<![endif]-->

<link type="text/css" rel="stylesheet" href="/resources/css/dropdown.css" />
<link type="text/css" rel="stylesheet" href="/resources/css/jquery.selectbox.css" />
<link rel="icon" href="/resources/images/favicon.ico" type="image/x-icon" />
    
<script type="text/javascript" src="/resources/js/lib/jquery/jquery-1.8.3.min.js"></script>
<script type="text/javascript" src="/resources/js/lib/jquery/jquery.validate.min-1.11.1.js"></script>
<script type="text/javascript" src="/resources/js/lib/jquery/additional-methods-1.11.1.min.js"></script>
<script type="text/javascript" src="/resources/js/custom/jquery-ui-1.9.2.custom.min.js"></script>
<script type="text/javascript" src="/resources/js/lib/json2/json2.min.js"></script>

<!-- Email regex validation is used globally in validation.js to check email address input. -->

<script type="text/javascript">
  MSISDN_MIN_LENGTH = 10;
  MSISDN_MAX_LENGTH = 11;
  VALID_EMAIL_ADDR_REGEX = /^\w+((-\w+)|(\.\w+))*\@[A-Za-z0-9]+((\.|-)[A-Za-z0-9]+)*\.[A-Za-z0-9]+$/;
</script>

<script type="text/javascript" src="/resources/js/custom/validation.js"></script>

<!-- TODO The ffg validation.js must be removed and only be included if needed -->
<script type="text/javascript" src="/resources/script/validation.js"></script>
<script type="text/javascript" src="/resources/script/common.js"></script>

<script type="text/javascript" src="/resources/js/lib/jquery/jquery.hoverIntent.r5.min.js"></script>
<script type="text/javascript" src="/resources/script/utility.js"></script><style type="text/css">input.styled { display: none; } select.styled { position: relative; width: 216px; opacity: 0; filter: alpha(opacity=0); z-index: 5; } .disabled { opacity: 0.5; filter: alpha(opacity=50); }</style><style type="text/css">input.styled2 { display: none; } select.styled2 { position: relative; width: 100px; opacity: 0; filter: alpha(opacity=0); z-index: 5; } .disabled { opacity: 0.5; filter: alpha(opacity=50); }</style>
<script type="text/javascript" src="/resources/js/lib/jquery/easyTooltip.1.0.min.js"></script>
<script type="text/javascript" src="/resources/js/lib/jquery/jquery.colorbox.1.3.18.min.js"></script>
<script type="text/javascript" src="/resources/script/swfobject.js"></script>
<!-- <script type="text/javascript" src="/resources/script/glossary.js"></script> -->
<script type="text/javascript" src="/resources/js/lib/jquery/jquery.lightbox.1.6.9.1.min.js"></script>

<script type="text/javascript" src="/resources/script/shopAccount.js"></script>

<script type="text/javascript" src="/resources/js/lib/jquery/jquery.li-scroller.1.0.js"></script>

<!--form -->
<script type="text/javascript" src="/resources/script/custom-form-elements.js"></script><style type="text/css">input.styledB { display: none; } select.styledB { position: relative; width: 190px; opacity: 0; filter: alpha(opacity=0); z-index: 5; } .disabled { opacity: 0.5; filter: alpha(opacity=50); }</style>
<script type="text/javascript" src="/resources/script/forms.js"></script>
<script type="text/javascript" src="/resources/js/lib/jquery/jquery.dropdown.3.3.min.js"></script>

<script type="text/javascript" src="/resources/js/lib/jquery/jquery.dataTables-1.10.13.min.js"></script>
<script type="text/javascript" src="/resources/js/lib/jquery/jquery.sticky.js"></script>
<script type="text/javascript" src="/resources/js/custom/paging-config.js"></script>

<script type="text/javascript" src="/resources/js/lib/jquery/textcounter.min.js"></script>


  <style type="text/css" id="vakata-context-stylesheet">.vakata-context { display:none; _width:1px; } .vakata-context, .vakata-context ul { margin:0; padding:2px; position:absolute; background:#f5f5f5; border:1px solid #979797; 	-moz-box-shadow:5px 5px 4px -4px #666666; -webkit-box-shadow:2px 2px 2px #999999; box-shadow:2px 2px 2px #999999; }.vakata-context ul { list-style:none; left:100%; margin-top:-2.7em; margin-left:-4px; } .vakata-context li.vakata-context-right ul { left:auto; right:100%; margin-left:auto; margin-right:-4px; } .vakata-context li { list-style:none; display:inline; }.vakata-context li a { display:block; padding:0 2em 0 2em; text-decoration:none; width:auto; color:black; white-space:nowrap; line-height:2.4em; 	-moz-text-shadow:1px 1px 0px white; -webkit-text-shadow:1px 1px 0px white; text-shadow:1px 1px 0px white; 	-moz-border-radius:1px; -webkit-border-radius:1px; border-radius:1px; }.vakata-context li a:hover { position:relative; background-color:#e8eff7; 	-moz-box-shadow:0px 0px 2px #0a6aa1; -webkit-box-shadow:0px 0px 2px #0a6aa1; box-shadow:0px 0px 2px #0a6aa1; }.vakata-context li.vakata-context-hover &gt; a { position:relative; background-color:#e8eff7; 	-moz-box-shadow:0px 0px 2px #0a6aa1; -webkit-box-shadow:0px 0px 2px #0a6aa1; box-shadow:0px 0px 2px #0a6aa1; }.vakata-context li a.vakata-context-parent { background-image:url("data:image/gif;base64,R0lGODlhCwAHAIAAACgoKP///yH5BAEAAAEALAAAAAALAAcAAAIORI4JlrqN1oMSnmmZDQUAOw=="); background-position:right center; background-repeat:no-repeat; } .vakata-context li.vakata-context-separator a, .vakata-context li.vakata-context-separator a:hover { background:white; border:0; border-top:1px solid #e2e3e3; height:1px; min-height:1px; max-height:1px; padding:0; margin:0 0 0 2.4em; border-left:1px solid #e0e0e0; _overflow:hidden; 	-moz-text-shadow:0 0 0 transparent; -webkit-text-shadow:0 0 0 transparent; text-shadow:0 0 0 transparent; 	-moz-box-shadow:0 0 0 transparent; -webkit-box-shadow:0 0 0 transparent; box-shadow:0 0 0 transparent; 	-moz-border-radius:0; -webkit-border-radius:0; border-radius:0; }.vakata-context li.vakata-contextmenu-disabled a, .vakata-context li.vakata-contextmenu-disabled a:hover { color:silver; background-color:transparent; border:0; box-shadow:0 0 0; }.vakata-context li a ins { text-decoration:none; display:inline-block; width:2.4em; height:2.4em; background:transparent; margin:0 0 0 -2em; } .vakata-context li a span { display:inline-block; width:1px; height:2.4em; background:white; margin:0 0.5em 0 0; border-left:1px solid #e2e3e3; _overflow:hidden; } .vakata-context-rtl ul { left:auto; right:100%; margin-left:auto; margin-right:-4px; } .vakata-context-rtl li a.vakata-context-parent { background-image:url("data:image/gif;base64,R0lGODlhCwAHAIAAACgoKP///yH5BAEAAAEALAAAAAALAAcAAAINjI+AC7rWHIsPtmoxLAA7"); background-position:left center; background-repeat:no-repeat; } .vakata-context-rtl li.vakata-context-separator a { margin:0 2.4em 0 0; border-left:0; border-right:1px solid #e2e3e3;} .vakata-context-rtl li.vakata-context-left ul { right:auto; left:100%; margin-left:-4px; margin-right:auto; } .vakata-context-rtl li a ins { margin:0 -2em 0 0; } .vakata-context-rtl li a span { margin:0 0 0 0.5em; border-left-color:white; background:#e2e3e3; } </style><style type="text/css" id="jstree-stylesheet">.jstree * { -webkit-box-sizing:content-box; -moz-box-sizing:content-box; box-sizing:content-box; }.jstree ul, .jstree li { display:block; margin:0 0 0 0; padding:0 0 0 0; list-style-type:none; list-style-image:none; } .jstree li { display:block; min-height:18px; line-height:18px; white-space:nowrap; margin-left:18px; min-width:18px; } .jstree-rtl li { margin-left:0; margin-right:18px; } .jstree &gt; ul &gt; li { margin-left:0px; } .jstree-rtl &gt; ul &gt; li { margin-right:0px; } .jstree .jstree-icon { display:inline-block; text-decoration:none; margin:0; padding:0; vertical-align:top; } .jstree .jstree-ocl { width:18px; height:18px; text-align:center; line-height:18px; cursor:pointer; vertical-align:top; } .jstree a { display:inline-block; line-height:16px; height:16px; color:black; white-space:nowrap; padding:1px 2px; margin:0; } .jstree a:focus { outline: none; } li.jstree-open &gt; ul { display:block; } li.jstree-closed &gt; ul { display:none; } .jstree a &gt; .jstree-checkbox { height:16px; width:16px; margin-right:1px; } .jstree-rtl a &gt; .jstree-checkbox { margin-right:0; margin-left:1px; } .jstree .jstree-check { margin:0; padding:0; border:0; display:inline; vertical-align:text-bottom; } #jstree-marker { position: absolute; top:0; left:0; margin:0; padding:0; border-right:0; border-top:5px solid transparent; border-bottom:5px solid transparent; border-left:5px solid; width:0; height:0; font-size:0; line-height:0; _border-top-color:pink; _border-botton-color:pink; _filter:chroma(color=pink); } #jstree-dnd { line-height:16px; margin:0; padding:4px; } #jstree-dnd .jstree-icon, #jstree-dnd .jstree-copy { display:inline-block; text-decoration:none; margin:0 2px 0 0; padding:0; width:16px; height:16px; } #jstree-dnd .jstree-ok { background:green; } #jstree-dnd .jstree-er { background:red; } #jstree-dnd .jstree-copy { margin:0 2px 0 2px; }.jstree a { text-decoration:none; } .jstree a &gt; .jstree-themeicon { height:16px; width:16px; margin-right:3px; } .jstree-rtl a &gt; .jstree-themeicon { margin-left:3px; margin-right:0; } .jstree .jstree-no-icons .jstree-themeicon, .jstree .jstree-themeicon-hidden { display:none; } .jstree .jstree-wholerow-ul { position:relative; display:inline-block; min-width:100%; }.jstree-wholerow-ul li &gt; a, .jstree-wholerow-ul li &gt; ins { position:relative; }.jstree-wholerow-ul .jstree-wholerow { width:100%; cursor:pointer; position:absolute; left:0; user-select:none;-webkit-user-select:none; -moz-user-select:none; -ms-user-select:none; }</style></head>
  <body><div id="cboxOverlay" style="display: none;"></div><div id="colorbox" class="" style="padding-bottom: 42px; padding-right: 42px; display: none;"><div id="cboxWrapper"><div><div id="cboxTopLeft" style="float: left;"></div><div id="cboxTopCenter" style="float: left;"></div><div id="cboxTopRight" style="float: left;"></div></div><div style="clear: left;"><div id="cboxMiddleLeft" style="float: left;"></div><div id="cboxContent" style="float: left;"><div id="cboxLoadedContent" style="width: 0px; height: 0px; overflow: hidden; float: left;"></div><div id="cboxLoadingOverlay" style="float: left;"></div><div id="cboxLoadingGraphic" style="float: left;"></div><div id="cboxTitle" style="float: left;"></div><div id="cboxCurrent" style="float: left;"></div><div id="cboxNext" style="float: left;"></div><div id="cboxPrevious" style="float: left;"></div><div id="cboxSlideshow" style="float: left;"></div><div id="cboxClose" style="float: left;"></div></div><div id="cboxMiddleRight" style="float: left;"></div></div><div style="clear: left;"><div id="cboxBottomLeft" style="float: left;"></div><div id="cboxBottomCenter" style="float: left;"></div><div id="cboxBottomRight" style="float: left;"></div></div></div><div style="position: absolute; width: 9999px; visibility: hidden; display: none;"></div></div>
    
    <div id="wrapper">
      
      
      
      
      
      










<script>
  $(window).load(function () {
    //$(".stickyVersion").sticky();
  });
</script>




  <p class="stickyVersion">Version : 4.2.1 - Build : 98723a0 - Config : 20181001-QA1</p>



  <div id="header">


    <!--Header Main-->
    <div id="header-main">

      <div id="logo"><img src="/resources/images/mtn_logo1.png" width="70" height="70" alt="MTN" />


      </div>

      <div id="country-change">
        <div class="country-details">
          <p>
          Welcome to <b>MyCustomer</b><br />
            Your contract self-service portal<br />
            Part of MTNSP<br />
            </p>
            <!-- br><br><br-->
        </div>
        <div style="clear:both"></div>
      </div>

      <!-- CSR Super Admin Directly -->
      
         












<ul id="menu">
  <li class="drop"><a href="/csr/home.htm" class="drop">Home</a>
  </li>

  <li class="drop"><a href="javascript:;" class="drop" id="menu-selected">Administration</a>
    <div class="dropDownMenu">
      <div class="navColumn navCol-services">
        <p class="columnHeader">CSR</p>
        <p class="columnItem"><a href="/csr/user/searchCSRs.htm?_HDIV_STATE_=22-0-4B111A7F802EF00C7A85CF0617D99ED0">Search</a></p>
        <p class="columnItem"><a href="/csr/user/displayAllCSRs.htm?_HDIV_STATE_=22-1-4B111A7F802EF00C7A85CF0617D99ED0">Display all</a></p>
      </div>

      <div class="navColumn-alt navCol-services">
        <p class="columnHeader">Organisation</p>
        <p class="columnItem"><a href="/csr/organisation/searchOrganisations.htm?_HDIV_STATE_=22-2-4B111A7F802EF00C7A85CF0617D99ED0">Search</a></p>
        <p class="columnItem"><a href="/csr/organisation/displayAllOrganisations.htm?_HDIV_STATE_=22-3-4B111A7F802EF00C7A85CF0617D99ED0">Display all</a></p>
        <p class="columnItem"><a href="/csr/organisation/createOrganisation.htm?_HDIV_STATE_=22-4-4B111A7F802EF00C7A85CF0617D99ED0">Add organisation</a></p>
      </div>

      <div class="navColumn navCol-services">
        <p class="columnHeader">Subscriber</p>
        <p class="columnItem"><a href="/csr/subscribers/search.htm?_HDIV_STATE_=22-5-4B111A7F802EF00C7A85CF0617D99ED0">Search</a></p>
        <p class="columnItem"><a href="/csr/subscribers/subscriberAgreement.htm?_HDIV_STATE_=22-6-4B111A7F802EF00C7A85CF0617D99ED0">Online subscriber agreement</a></p>
      </div>

      <div class="navColumn-alt navCol-services">
        <p class="columnHeader">SIM price</p>
        <p class="columnItem"><a href="/csr/charge/changeSimPriceDisplay.htm?_HDIV_STATE_=22-7-4B111A7F802EF00C7A85CF0617D99ED0">SIM price</a></p>
      </div>

    </div>
  </li>

  <li class="drop"><a href="javascript:;" class="drop">Reports</a>
    <div class="dropDownMenu">

      <div class="navColumn navCol-services">
        <p class="columnHeader">Transactions</p>
        <p class="columnItem"><a href="/csr/eventhistory/search.htm">Transaction events</a></p>
        <p class="columnItem"><a href="/csr/usereventhistory/search.htm">User events</a></p>
      </div>

    </div>
  </li>

  <li class="drop"><a href="javascript:;" class="drop">Support</a>
    <div class="dropDownMenu">

      <div class="navColumn navCol-services">
        <p class="columnHeader">Communication</p>
        <p class="columnItem"><a href="" target="new">Remedy ticket</a></p>
        <p class="columnItem"><a href="/csr/communication/retrieveAllCommMessages.htm?_HDIV_STATE_=22-8-4B111A7F802EF00C7A85CF0617D99ED0">Message</a></p>
        <p class="columnItem"><a href="/csr/ticker/retrieveAllTickers?_HDIV_STATE_=22-9-4B111A7F802EF00C7A85CF0617D99ED0">Ticker</a></p>
      </div>

      <div class="navColumn-alt navCol-services">
        <p class="columnHeader">Settings</p>
        <p class="columnItem"><a href="/csr/settings/secure/view-types.htm?_HDIV_STATE_=22-10-4B111A7F802EF00C7A85CF0617D99ED0">MTN secure</a></p>
      </div>
    </div>
   </li>

</ul>

      

      <!-- CSR Admin Directly -->
      

      <!-- CSR Super Agent Directly -->
      

      <!-- CSR Agent Directly -->
      

      <div style="clear:both"></div>
      <!-- Navigation Ends -->
    </div>
  </div> <!-- id="header" -->
      <div id="content">
        










<!-- Print / Logout-->
<div id="purchase-utility">
  <div class="purchase-utility-details">
    <a href="#" class="printBt" onclick="window.print();">Print</a> |
    <a href="/landing/security_logout">Log out</a>
    
  </div>
</div>
<!-- Breadcrumb -->
<div id="breadcrumb">
  You are here:
  <span id="breadcrumb-content">
  
    Administration
  
  
    | Subscriber
  
  
    | Search
  
  </span>
</div>
        































<script type="text/javascript">
MSISDN_MIN_LENGTH = 10;
MSISDN_MAX_LENGTH = 15;
ACCOUNT_LENGTH = 10;
SIM_NUMBER_LENGTH = 13;
RSA_IDENTITY_NUMBER_LENGTH = 13;
</script>

<script type="text/javascript" src="/resources/script/subscriber-search.js"></script>

<div id="col-left">
  

  <h1>Subscriber search</h1>

  <form id="form" action="/csr/subscribers/results.htm" method="GET" novalidate="novalidate">
    
    <div id="info-table">
      <table class="table" id="form" border="0" cellpadding="0" cellspacing="0" width="100%">
        <tbody>
          <tr>
            <td class="left">Search type</td>
              <td class="right">
                <ul class="filterList">
                  <li class="radioList">
                <span class="radio" style="background-position: 0px 0px;"></span><input id="searchType1" name="searchType" class="styledB" type="radio" value="0" checked="checked" />
                    <span>Cellphone number</span>
                  </li>
                  <li class="radioList">
                    <span class="radio" style="background-position: 0px -50px;"></span><input id="searchType2" name="searchType" class="styledB" type="radio" value="1" />
                    <span>Account number</span>
                  </li>
                 <li class="radioList">
                    <span class="radio" style="background-position: 0px 0px;"></span><input id="searchType3" name="searchType" class="styledB" type="radio" value="2" />
                    <span>SIM number</span>
                 </li>
                  <li class="radioList" style="border-bottom: none;">
                    <span class="radio" style="background-position: 0px 0px;"></span><input id="searchType4" name="searchType" class="styledB" type="radio" value="3" />
                      <span>South African ID number</span>
                   </li>
             </ul>
              </td>
          </tr>

         <tr class="HIDE_ROW CELLPHONE" style="display: none;">
          <td class="left">Cellphone number</td>
          <td class="right search-criteria">
            <input id="msisdn" name="msisdn" class="textfield activeInput oneRequired DISABLE_ROW CELLPHONE " type="text" value="" disabled="disabled" />
            <div id="validmsisdn" class="formValidateImg validationCheck"></div>
          </td>
        </tr>
        <tr class="HIDE_ROW ACCOUNT" style="">
          <td class="left">Account number</td>
          <td class="right search-criteria">
            <input id="accountNo" name="accountNo" class="textfield activeInput oneRequired DISABLE_ROW ACCOUNT" type="text" value="" maxlength="10" />
            <div id="validaccountNo" class="formValidateImg validationCheck"></div>
          </td>
        </tr>
        <tr class="HIDE_ROW SIM" style="display: none;">
          <td class="left">SIM number</td>
          <td class="right search-criteria">
            <input id="simNo" name="simNo" class="textfield activeInput oneRequired DISABLE_ROW SIM" type="text" value="" maxlength="13" disabled="disabled" />
            <div id="validsimNo" class="formValidateImg validationCheck"></div>
          </td>
        </tr>
        <tr class="HIDE_ROW ID" style="display: none;">
          <td class="left">South African ID number</td>
           <td class="right search-criteria">
            <input id="idNo" name="idNo" class="textfield activeInput oneRequired DISABLE_ROW ID" type="text" value="" maxlength="13" disabled="disabled" />
            <div id="valididNo" class="formValidateImg validationCheck"></div>
          </td>
        </tr>

        </tbody>
      </table>
    </div>

    <br />

    <div class="formSubmit">
      
      <ul>
        
        <li class="clearForm"><a class="clearBt" id="resetForm" onclick="window.location.reload();">Clear</a></li>
        <li><button id="submitForm" name="submitForm" class="login_btn button IE" type="submit" value="0">Submit</button></li>
        </ul>
        <div style="clear: both;"></div>
      </div>
  <input type="hidden" name="_HDIV_STATE_" value="22-11-3D11E4722AE4408CB5670825FCDDAE6A" />
</form>
</div>

        





























<script type="text/javascript" src="/resources/js/lib/jquery/jstree-v.1.0/jstree.js"></script>

<script type="text/javascript">
  $(document).ready(function () {
    $(".tciframe").colorbox({iframe: true, innerWidth: 650, innerHeight: 500});
  });
</script>

<div id="col-right">

  <noscript>
  &lt;div class="banner"&gt;
    &lt;h3 style="font-size: 15px;"&gt;&lt;img src="/resources/images/error.png" /&gt;You do not have Javascript enabled. &lt;br&gt;This site requires Javascript to be enabled to work optimally.&lt;/h3&gt;
  &lt;/div&gt;

  </noscript>

  <div id="career-search">
    <h2 class="rightColHeader">My <span class="pageHeading-h2-light">profile</span></h2>
    <div class="career_search_drop">

      <script type="text/javascript">
        $(function () {
          $.ajax({
            url: '/csr/showWelcomeMessage',
            success: function (result) {
              $("#showWelcomeMessage").html("");
              if (result) {
                $("#showWelcomeMessage").html('&lt;h4&gt;Welcome, &lt;span&gt;' + result + '&lt;/span&gt;&lt;/h4&gt;&lt;h4&gt;You last logged in on 2018-10-12 at 12:22:31&lt;/h4&gt;');
              }
            }
          });
        });
      </script>

      <div id="showWelcomeMessage"></div>


      <div style="clear:both; height:20px"></div>

      <div id="info-table">
        <table border="0" cellpadding="0" cellspacing="0" width="100%">
          <tbody>
            <tr>
              <td class="rowKey" width="50%" align="left" valign="top">Name</td>
              <td width="50%" align="right" valign="top">Charmaine Abiram</td>
            </tr>
            <tr class="row_alt">
              <td class="rowKey" width="50%" align="left" valign="top">Username</td>
              <td width="50%" align="right" valign="top">abiram_c_103332</td>
            </tr>
            <tr>
              <td class="rowKey" width="50%" align="left" valign="top">Role</td>
              <td width="50%" align="right" valign="top">CSR Super Administrator</td>
            </tr>
          </tbody>
        </table>
        <br />
      </div>
      <a href="/landing/security_logout"><span style="float:right">Log out</span></a>
      <div style="clear:both; height:0px"></div>
    </div>
  </div>

  <div style="clear:both; height:20px"></div>

  <div style="clear:both; height:20px"></div>
  <div class="banner">
    <img src="/resources/images/note_icon.png" width="71" height="76" alt="Note" />
    <h2>Note</h2>
    All transactions are subject to the <a class="al tciframe cboxElement" href="/subscriber/tc/susbcriberTC.htm?lang=en">MTN SP Subscriber Terms and Conditions</a>
  </div>

  <div class="banner">
    <img src="/resources/images/help_icon.png" alt="Note" />
    <h2>Help</h2>
    Search for MTN contract subscribers using any one of the criteria provided
  </div>

  <script type="text/javascript">
    $(function () {

      $.ajax({
        url: '/landing/marketing/retrieveMarketingLink',
        success: function (result) {
          $("#marketingAds").html("");
          if (result.length != 0) {
            $("#marketingAds").html('&lt;a target="new" href="' + result.marketingAdURL + '"&gt; &lt;img title="" src="' + result.marketingAdImageLocation + '"&gt; &lt;/a&gt;');
          }
        }
      });
    });
  </script>
  <div id="marketingAds"><a target="new" href="http://www.mtn.co.za/"> <img title="" src="/resources/images/marketing-banner-1.gif" /> </a></div>

</div>


        



















<div style="clear:both"></div>
<div id="footer">
  <!-- External Navigation -->
  <div id="external-nav">
    <div id="social">
      <a href="http://www.facebook.com/MTNza" class="facebook greyTip" title="Click here to visit the MTN Facebook page" target="_blank"></a>
      <a href="http://www.twitter.com/MTNza" class="twitter greyTip" title="Click here to visit the MTN Twitter page" target="_blank"></a>
      <a href="http://www.youtube.com/user/MTNza" class="youtube greyTip" title="Click here to visit MTN's YouTube channel" target="_blank"></a>
      <a href="http://www.mtnblog.co.za/" class="blog greyTip" title="Click here to visit the MTN blog" target="_blank"></a>
    </div>
    <div id="external-menu">
      <p>Other Sites</p>
      <div>
        <dl id="sample" class="dropdown">
          <dt><a href="javascript:;" class="dropdownSelected"><span>Select</span></a></dt>
          <dd>
            <ul style="display: none;">
              <li><a href="http://www.mtnfootball.co.za" target="_blank">MTN Football</a></li>
              <li><a href="http://www.mtnplay.co.za" target="_blank">MTN Play</a></li>
              <li><a href="http://www.mtnbusiness.co.za" target="_blank">MTN Business</a></li>
              <li><a href="http://www.mtn.com" target="_blank">MTN Group</a></li>
              <li><a href="http://www.mtnlotto.co.za" style="border:none;" target="_blank">MTN Lotto</a></li>
            </ul>
          </dd>
        </dl>
      </div>

    </div>
    <div style="clear:both;"></div>
  </div>
 
 <!-- CSR Super Admin Directly -->
  
     










<div id="footer-nav" class="footerAuto">
  <div class="footer-nav-col footerNav">
    <h3>Administration</h3>
    <ul>
      <li><a href="/csr/user/searchCSRs.htm?_HDIV_STATE_=22-13-4B111A7F802EF00C7A85CF0617D99ED0">CSR: Search</a></li>
      <li><a href="/csr/user/displayAllCSRs.htm?_HDIV_STATE_=22-14-4B111A7F802EF00C7A85CF0617D99ED0">CSR: Display all</a></li>
      

      <li><a href="/csr/organisation/searchOrganisations.htm?_HDIV_STATE_=22-15-4B111A7F802EF00C7A85CF0617D99ED0">Organisation: Search</a></li>
      <li><a href="/csr/organisation/displayAllOrganisations.htm?_HDIV_STATE_=22-16-4B111A7F802EF00C7A85CF0617D99ED0">Organisation: Display all</a></li>
      <li><a href="/csr/organisation/createOrganisation.htm?_HDIV_STATE_=22-17-4B111A7F802EF00C7A85CF0617D99ED0">Organisation: Add organisation</a></li>

      <li><a href="/csr/subscribers/search.htm?_HDIV_STATE_=22-18-4B111A7F802EF00C7A85CF0617D99ED0">Subscriber: Search</a></li>
      <li><a href="/csr/subscribers/subscriberAgreement.htm?_HDIV_STATE_=22-19-4B111A7F802EF00C7A85CF0617D99ED0">Subscriber: Online subscriber agreement</a></li>
      <li><a href="/csr/charge/changeSimPriceDisplay.htm?_HDIV_STATE_=22-20-4B111A7F802EF00C7A85CF0617D99ED0">SIM price</a></li>
      <li> </li>
    </ul>
  </div>

  <div class="footer-nav-col footerNav">
    <h3>Reports</h3>
    <ul>
      <li><a href="/csr/usereventhistory/search.htm">User events</a></li>
      <li><a href="/csr/eventhistory/search.htm">Transaction events</a></li>
    </ul>
  </div>

  <div class="footer-nav-col footerNav">
      <h3>Support</h3>
      <ul>
        <li><a href="" target="new">Remedy ticket</a></li>
        <li><a href="/csr/ticker/retrieveAllTickers?_HDIV_STATE_=22-21-4B111A7F802EF00C7A85CF0617D99ED0">Ticker</a></li>
        <li><a href="/csr/communication/retrieveAllCommMessages.htm?_HDIV_STATE_=22-22-4B111A7F802EF00C7A85CF0617D99ED0">Message</a></li>
        <li><a href="/csr/settings/secure/view-types.htm?_HDIV_STATE_=22-23-4B111A7F802EF00C7A85CF0617D99ED0">MTN secure</a></li>
        <li> </li>
        <li> </li>
        <li> </li>
        <li> </li>
      </ul>
    </div>
  </div>

  
  
  <!-- CSR Admin Directly -->
  
  
  <!-- CSR Super Agent Directly -->
  
  
  <!-- CSR Agent Directly -->
  
       
  <!-- Copyright Navigation -->
  <div id="copyright-nav">
    <a href="https://www.mtn.co.za/Pages/Contact-us.aspx" target="_blank">Contact us</a> |
    <a href="https://www.mtn.co.za/About_us/Pages/Overview.aspx" target="_blank">About us</a> |
    <a href="https://www.mtn.co.za/Pages/Sponsorship.aspx" target="_blank">Sponsorships</a> |
    <a href="https://www.mtn.co.za/partners/pages/partners.aspx" target="_blank">Partners</a> |
    <a href="https://www.mtn.co.za/Pages/Press.aspx" target="_blank">Press and Media</a> |
    <a href="https://www.mtn.co.za/Careers/pages/overview.aspx" target="_blank">Careers</a> |
    <a href="https://www.mtn.co.za/Pages/Legal.aspx" target="_blank">Legal</a>
    <div id="copyright" target="_blank">Copyright © 2011 Mobile Telephone Networks. All rights reserved.</div>
  </div>
</div>



<script type="text/javascript">
var GlossaryJS_section = "MSU";
</script>

<script>
/* Modal */
$(document).ready(function(){
  $(".change-country").colorbox({iframe:true, innerWidth:"665", innerHeight:"470"});
});
/* Tooltip */
$(document).ready(function(){
  $(".greyTip").easyTooltipGrey();

});
$(document).ready(function() {
//Dropdown
  $("#Continent, #Country, #Services_filter, .styled, .comboResults, .styled2").msDropDown();
});
</script>
















<script type="text/javascript">

  isEnabled = false;
  analyticsAccountNo = "UA-11133282-26";

  if (isEnabled) {
    var _gaq = _gaq || [];
    var pluginUrl = '//www.google-analytics.com/plugins/ga/inpage_linkid.js';
    _gaq.push(['_require', 'inpage_linkid', pluginUrl]);
    _gaq.push(['_setAccount', analyticsAccountNo]);
    _gaq.push(['_setDomainName', 'mtn.co.za']);
    _gaq.push(['_trackPageview']);

    (function() {
      var ga = document.createElement('script');
      ga.type = 'text/javascript';
      ga.async = true;
      ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
      var s = document.getElementsByTagName('script')[0];
      s.parentNode.insertBefore(ga, s);
    })();
  }

</script>


      </div>
    </div>
  
<div id="1539339767643" class="jquery-lightbox-overlay" style="position: fixed; top: 0px; left: 0px; opacity: 0.6; display: none; z-index: 99998;"></div><div class="jquery-lightbox-move" style="position: absolute; z-index: 99999; top: -999px;"><div class="jquery-lightbox jquery-lightbox-mode-image"><div class="jquery-lightbox-border-top-left"></div><div class="jquery-lightbox-border-top-middle"></div><div class="jquery-lightbox-border-top-right"></div><a class="jquery-lightbox-button-close" href="#close"><span>Close</span></a><div class="jquery-lightbox-navigator"><a class="jquery-lightbox-button-left" href="#"><span>Previous</span></a><a class="jquery-lightbox-button-right" href="#"><span>Next</span></a></div><div class="jquery-lightbox-buttons"><div class="jquery-lightbox-buttons-init"></div><a class="jquery-lightbox-button-left" href="#"><span>Previous</span></a><a class="jquery-lightbox-button-max" href="#"><span>Maximize</span></a><div class="jquery-lightbox-buttons-custom"></div><a class="jquery-lightbox-button-right" href="#"><span>Next</span></a><div class="jquery-lightbox-buttons-end"></div></div><div class="jquery-lightbox-background"></div><div class="jquery-lightbox-html"></div><div class="jquery-lightbox-border-bottom-left"></div><div class="jquery-lightbox-border-bottom-middle"></div><div class="jquery-lightbox-border-bottom-right"></div></div></div><ul class="vakata-context"></ul><div id="jstree-marker" style="display: none;"> </div></body></html>